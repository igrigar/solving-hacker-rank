#!/usr/bin/env python
# -*- coding: utf8 -*-

if __name__ == '__main__':
    min_1 = 99999999999999
    min_2 = 99999999999999

    n = []

    for _ in range(int(input())):
        name =  input()
        score = float(input())

        if score < min_1:
            min_2 = min_1
            min_1 = score
        elif score < min_2:
            min_2 = score
            n = [name]
        elif score == min_2:
            n += [name]

        print("min 1: %f, min_2: %f" % (min_1, min_2))
