# Solving Hacker Rank

Solution to the various challenges defined in the page HackerRank
(https://www.hackerrank.com/). All the challenges are organized in folders
following the organization established in the page.

## Security

    * Security Functions
    * Security Functions II
    * Security Bijective Functions
    * Security Function Inverses
    * Security Permutations
    * Security Involution
    * Security - Message Space and Ciphert Space
    * Security Key Spaces
    * Security Encryption Scheme
    * Keyword Transposition Cipher

## Python

### Introduction

### Basic Data Types
