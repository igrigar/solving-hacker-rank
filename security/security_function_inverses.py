#!/usr/bin/env python
# -*- coding: utf8 -*-

n = int(input().strip())
f = [int(x) for x in input().strip().split(' ')]

for i in range(1, 21):
    try:
        print(f.index(i) + 1)
    except:
        continue
