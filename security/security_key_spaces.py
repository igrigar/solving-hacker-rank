#!/usr/bin/env python
# -*- coding: utf8 -*-

p = [int(x) for x in list(str(int(input().strip())))]
k = int(input().strip())
print(int("".join([str((i+k)%10) for i in p])))
