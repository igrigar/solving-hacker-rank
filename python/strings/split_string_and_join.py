#!/usr/bin/env python
# -*- coding: utf8 -*-

def split_and_join(line):
    return "-".join(line.split(" "))

if __name__ == '__main__':
    # Your code here
    line = input()
    result = split_and_join(line)
    print(result)
