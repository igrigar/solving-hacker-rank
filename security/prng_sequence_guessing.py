#!/usr/bin/env python
# -*- coding: utf8 -*-

seed = 93142

def next_int(n):
    global seed
    seed = (seed * 0x5DEECE66D + 0xB) & ((1 << 48) - 1)
    return int((seed >> 17) % n)

n = int(input().strip())
c = [int(i) for i in input().strip().split(" ")]


print(next_int(1000))
print(next_int(1000))
print(next_int(1000))
