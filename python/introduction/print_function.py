#!/usr/bin/env python
# -*- coding: utf8 -*-

if __name__ == '__main__':
    n = int(input())
    r = 0

    for i in range(1, n + 1):
        p = 1
        i_ = i

        while i_ > 0:
            p *= 10
            i_ //= 10

        r *= p
        r += i

    print(r)
