#!/usr/bin/env python
# -*- coding: utf8 -*-

correction = 65
alphabet = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']

n = int(input().strip())
o = ""

for case in range(0, n):

    k = list(dict.fromkeys(list(input().strip())))
    c = input().strip().split(" ")

    _sm = list(dict.fromkeys(k + alphabet))

    sm = []
    for a in sorted(k):
        i = _sm.index(a)
        while i < len(_sm):
            sm.append(_sm[i])
            i += len(k)
    o += " ".join(["".join([chr(sm.index(c) + correction) for c in w]) for w in c]) + "\n"

print(o.strip())
