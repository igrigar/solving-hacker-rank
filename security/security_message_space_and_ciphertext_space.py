#!/usr/bin/env python
# -*- coding: utf8 -*-

p = [x for x in list(input().strip())]
print("".join([str((int(i)+1)%10) for i in p]))

