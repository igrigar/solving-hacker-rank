#!/usr/bin/env python
# -*- coding: utf8 -*-

if __name__ == '__main__':
    a = int(input())
    b = int(input())

    print("%d\n%d\n%d" % (a+b, a-b, a*b))
