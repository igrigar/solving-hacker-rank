#!/usr/bin/env python
# -*- coding: utf8 -*-

if __name__ == '__main__':
    N = int(input())
    n = []

    for _ in range(0, N):
        i = [i for i in input().split(" ")]

        if i[0] == "insert":
            n.insert(int(i[1]), int(i[2]))
        elif i[0] == "print":
            print(n)
        elif i[0] == "remove":
            n.remove(int(i[1]))
        elif i[0] == "append":
            n.append(int(i[1]))
        elif i[0] == "sort":
            n = sorted(n)
        elif i[0] == "pop":
            n.pop()
        elif i[0] == "reverse":
            n.reverse()
