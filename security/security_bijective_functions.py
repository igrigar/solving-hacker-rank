#!/usr/bin/env python
# -*- coding: utf8 -*-

n = int(input().strip())
f = [int(x) for x in input().strip().split(' ')]

if len(f) > len(set(f)):
    print ("NO")
else:
    print ("YES")
