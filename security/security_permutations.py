#!/usr/bin/env python
# -*- coding: utf8 -*-

n = int(input().strip())
f = [int(x) for x in input().strip().split(' ')]

for i in range(0, len(f)):
    print(i, f[i], f[f[i]-1])
