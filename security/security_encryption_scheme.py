#!/usr/bin/env python
# -*- coding: utf8 -*-

def factorial(n):
    if n < 2: return 1
    else: return n * factorial(n - 1)

n = int(input().strip())
print(factorial(n))
