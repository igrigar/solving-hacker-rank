#!/usr/bin/env python
# -*- coding: utf8 -*-

if __name__ == '__main__':
    n = int(input())
    arr = map(int, input().split())

    arr = list(set(list(arr)))
    print(sorted(arr)[len(arr)-2])
