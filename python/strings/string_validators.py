#!/usr/bin/env python
# -*- coding: utf7 -*-

if __name__ == '__main__':
    s = input()
    print(s)
    print(s.isalnum())
    print(s.isalnum() and not s.isdigit())
    print(s.isalnum() and not s.isalpha())
    print(s.isalnum() and not s.isdigit() and not s.isupper())
    print(s.isalnum() and not s.isdigit() and not s.islower())
